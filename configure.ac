## Process this file with autoconf to produce a configure script

AC_PREREQ([2.69])
AC_INIT([libxspf], [1.2.1])
AC_CONFIG_SRCDIR([src/XspfReader.cpp])
AC_CONFIG_AUX_DIR([build-aux])
AC_CONFIG_MACRO_DIRS([m4])
AM_INIT_AUTOMAKE([1.16 foreign dist-zip dist-bzip2 no-dist-gzip subdir-objects])

AC_PROG_CC
AC_PROG_CXX
AM_PROG_AR
AC_HEADER_STDC

PKG_PROG_PKG_CONFIG
PKG_INSTALLDIR
LT_INIT



## Options
AC_ARG_ENABLE([test],
	AS_HELP_STRING([--disable-test], [disable 'make check' and libcpptest dependency]))
AM_CONDITIONAL([XSPF_TEST_ENABLED], [test "x${enable_test}" != "xno"])


AC_ARG_ENABLE([doc],
	AS_HELP_STRING([--enable-doc], [generate API documentation with Doxygen]))
AM_CONDITIONAL([GENERATE_DOC], [test "x${enable_doc}" = "xyes"])

AS_IF([test "x${enable_doc}" = "xyes"], [
	AC_CONFIG_SUBDIRS([doc])
	AC_CONFIG_SUBDIRS([bindings/c/doc])
])



## Find dependencies
PKG_CHECK_MODULES([EXPAT], [
	expat >= 1.95.8
], [], [
	AC_MSG_ERROR([Please install expat 1.95.8 or later.])
])

PKG_CHECK_MODULES([URIPARSER], [
	liburiparser >= 0.7.5
], [], [
	AC_MSG_ERROR([Please install uriparser 0.7.5 or later.])
])

AS_IF([test "x${enable_test}" != "xno"], [
	PKG_CHECK_MODULES([CPPTEST], [
		libcpptest >= 1.1.0
	], [], [
		AC_MSG_ERROR([Please install libcpptest 1.1.0 or later.])
	])
])



## Create output files
AC_CONFIG_FILES([
	xspf.pc
	Makefile
])
AC_OUTPUT

AC_MSG_RESULT([
===========================================================================
Configuration
  Prefix ........... ${prefix}
  Test suite ....... ${enable_test:-yes}
  Documentation .... ${enable_doc:-no}

Continue with
  make
  make check
  sudo make install
])
