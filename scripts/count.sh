#! /usr/bin/env bash
PWD_BACKUP=${PWD}
SCRIPT_DIR=`dirname "${PWD}/$0"`
cd "${SCRIPT_DIR}/.." || exit 1
function fail() { cd "${PWD_BACKUP}" ; exit 1; }
####################################################################

FILES_INCLUDE=`find ./include -name '*.h'`
FILES_SOURCE=`find ./src -name '*.cpp'`
FILES_TEST_1=`find ./test -maxdepth 1 -name '*.h'`
FILES_TEST_2=`find ./test -maxdepth 1 -name '*.cpp'`
FILES_TEST_3=`find ./test/ProjectOpus \( -name '*.cpp' -o -name '*.h' \)`
FILES="${FILES_INCLUDE} ${FILES_SOURCE} ${FILES_TEST_1} ${FILES_TEST_2} ${FILES_TEST_3}"
SUM_FILES=`echo $FILES | wc --words`
SUM_LINES=`cat $FILES | wc --lines`
SUM_BYTES=`cat $FILES | wc --bytes`
echo "${SUM_BYTES} bytes, ${SUM_LINES} lines, ${SUM_FILES} files"

####################################################################
cd "${PWD_BACKUP}" || fail
exit 0
