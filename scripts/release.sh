#! /usr/bin/env bash
(
cd $(dirname $(which "$0"))/.. || exit 1
####################################################################
package=libxspf
####################################################################


echo ========== cleanup ==========
rm -vf ${package}-*.{tar.*,zip} 2> /dev/null
rm -vRf ${package}-* 2> /dev/null

echo
echo ========== bootstrap ==========
./autogen.sh --download || exit 1

echo
echo ========== configure ==========
./configure --enable-doc || exit 1

echo
echo ========== make distcheck ==========
make -j10 distcheck DISTCHECK_CONFIGURE_FLAGS='--enable-doc' || exit 1

echo
echo ========== package docs ==========
./doc/release.sh || exit 1
./bindings/c/doc/release.sh || exit 1

####################################################################
)
res=$?
[ $res = 0 ] || exit $res

cat <<'CHECKLIST'

Fine.


Have you
* run ./scripts/edit_version.sh
* updated the soname
* updated file lists
  - lazy header (Xspf.h)
  - Makefile.am
  - Code::Blocks
  - Visual Studio 2005
* searched for TODO inside code using
  grep -R 'TODO' include/* src/* test/*
?

If so ..
* upload release with ReleaseForge
* announce through ..
  - Blog
  - Mailing lists
  - Freshmeat
  - SourceForge news
* upload doc
* update doc to website
* git tag

CHECKLIST
exit $res
